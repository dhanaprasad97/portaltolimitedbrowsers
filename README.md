# README #

To check the accessibility whether portal can be used on the supported browsers, devices and operating systems by utilizing javascript functionalities and in case of uneligible redirects to error page


### How do I get set up? ###

* Deploy the LWC component present in this Asset "CheckSupportedBrowsers"
* Make sure its is exposed to communities
* Include this component in Header part of community.
* Create a new page in community named "Browser Error" and add a rich text content header with message to say unsupported browser.
* Publish the communnity
* When you try to utilize this portal from unsupported browser redirects to Error page named "Browser Error" 
