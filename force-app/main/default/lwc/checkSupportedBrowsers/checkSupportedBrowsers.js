import { LightningElement } from 'lwc';
/**Created by : Dhana Prasad */
/** To check browser and device used and allow or decline by redirecting to error page */
export default class CheckSupportedBrowsers extends LightningElement {
    connectedCallback() {
        if (window.location.href.search('browser-error') == -1) {
            console.log('test');
            if (this.loginAccessEligible()) {
                this.dispatchEvent(new CustomEvent("eligible", { detail: true }));// custom event if included as child event
                console.log(' eligible');
            } else {
                this.dispatchEvent(new CustomEvent("eligible", { detail: false })); // custom event if included as child event
                console.log('not eligible');
                window.open("/MHD/s/browser-error", "_self"); // redirection to error page on salesforce community
            }
        }
    }

    // to check if portal accessible from the used browser, device and operating system
    loginAccessEligible() {
        var deviceType = this.getDeviceType();

        // Opera 8.0+
        var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

        // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';

        // Safari 3.0+ "[object HTMLElementConstructor]" 
        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

        // Internet Explorer 6-11
        var isIE = /*@cc_on!@*/false || !!document.documentMode;

        // Edge 20+
        var isEdge = !isIE && !!window.StyleMedia;

        // Chrome 1 - 71
        var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        // Blink engine detection
        var isBlink = (isChrome || isOpera) && !!window.CSS;
        var osName = "Unknown OS";
        if (navigator.userAgent.indexOf("Win") != -1) osName =
            "Windows OS";
        if (navigator.userAgent.indexOf("Mac") != -1) osName =
            "Macintosh";
        if (navigator.userAgent.indexOf("Linux") != -1) osName =
            "Linux OS";
        if (navigator.userAgent.indexOf("Android") != -1) osName =
            "Android OS";
        if (navigator.userAgent.indexOf("like Mac") != -1) osName =
            "iOS";
        if ((/windows phone/i.test(userAgent))) osName =
            "Windows Phone";

        // check browsers, os and device whether to allow or not
        if((osName == 'Macintosh' && deviceType == 'desktop' && (isSafari || isChrome ||  isFirefox) )||
        (osName == 'Windows OS'&& deviceType == 'desktop' && (isChrome || isEdge ||  isFirefox)) ||
        (osName == 'Andriod OS'&& (deviceType == 'phone' || deviceType == 'tablet') && (isChrome)) ||
        (osName == 'iOS'&& (deviceType == 'phone' || deviceType == 'tablet') && (isChrome)) ||
        (osName == 'Windows Phone'&& (deviceType == 'phone' || deviceType == 'tablet') && (isEdge))
        ){
             return  true;
        }   
        return false;
    }


    // to get device type  used by portal 
    getDeviceType() {
        const ua = navigator.userAgent;
        if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
            return "tablet";
        }
        if (
            /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
                ua
            )
        ) {
            return "mobile";
        }
        return "desktop";
    }

}